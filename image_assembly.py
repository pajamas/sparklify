from PIL import Image
import image_processing as processing

_LIGHTEN_INTENSITY = 5

def lighten(image: Image, mask: Image = None):
    """Applies the default lightening filter to the image in accordance with
    the given mask."""

    def make_lighten_filter(relative_intensity):
        return lambda pixel: pixel + _LIGHTEN_INTENSITY*relative_intensity

    rg_filter = make_lighten_filter(1)
    b_filter = make_lighten_filter(0.5)
    filters = {
        "R": rg_filter,
        "G": rg_filter,
        "B": b_filter
    }

    lightened_image = processing.apply_filters_to_channels(image, filters)

    if not mask:
        return lightened_image

    return processing.merge_images_by_mask(image, lightened_image, mask)

def add_sparkles_to_image(image: Image, gif_frame: Image, mask: Image = None):
    """Adds a frame of sparkles to an image.

    Note:
        1. The frame can have a black or transparent background
        2. Channels other than red will be ignored, so colors in the frame will
        not be preserved
    """
    
    transparent_gif = processing.convert_red_to_alpha(gif_frame)
    image_with_gif_texture = processing.add_texture(image, transparent_gif)
    
    if mask:
        image = processing.merge_images_by_mask(image, image_with_gif_texture, mask)
    else:
        image = image_with_gif_texture

    return image

def add_gif_to_frames(frames: list[Image], gif: Image, mask: Image) -> list[Image]:
    for i in range(len(frames)):
        gif.seek(i%gif.n_frames)
        frames[i] = add_sparkles_to_image(frames[i], gif, mask)

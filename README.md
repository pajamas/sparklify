![Sparklify](resources/sparklify.webp)

---

A tool for making images into sparkly gifs/webps

## Help

### Usage

```
usage: ./sparklify [-h] [-f {webp,gif}] [-o FILENAME] [-m MASK] [-d] [-r RESIZE] [-n] [-b] image_path

A program for making images into sparkly gifs

positional arguments:
  image_path

options:
  -h, --help            show this help message and exit
  -f {webp,gif}, --format {webp,gif}
                        output file format, either webp or gif
  -o FILENAME, --output-file FILENAME
                        name of output file
  -m MASK, --mask MASK  path to mask file
  -d, --draw-mask       draw mask
  -r RESIZE, --resize RESIZE
                        ratio for resizing input image.
  -n, --no-lighten      don't lighten sparkly parts of image
  -b, --bubbles         add bubbles to top half of image

https://gitlab.com/pajamas/sparklify
```

### How to sparklify

1. If you don't want sparkles everywhere:
    - Option 1: Use the -d flag and draw where you want the sparkles to be.
    - Option 2: Take your original image and, with your image editing program
      of choice, make a mask version, that is the same size as the original. It
      should be black and white (gray allowed), where black parts will not have
      sparkles and white parts will have the brightest sparkles. Use the -m
      flag to specify the path to the mask file.

2. Decide the output file format. Default is webp, if you want a gif 
then use the flag `-f gif`.

3. If your image is large (larger than 500x500, for example), you
might want to resize it. Use the -r flag and give it a value that you
want the width and height to be multiplied by. For example, if you want
the result to be half as tall and wide as the original, use `-r 0.5`.

4. Do the sparklify command.

### Problems

!!! If your output gif has white flashes, you may need to update pillow:

```sh
pip install --upgrade pillow
```

!!! Sometimes saving as gif can be buggy. In case your gif doesn't look right,
you can save it as webp first, and then convert it to a gif with ImageMagick
with this command:

```sh
convert sparkle.webp sparkle.gif
```

If you don't have the `convert` command, it comes with the apt package
`imagemagick-6.q16` (on Ubuntu).

## Examples

Unedited image:

![Unedited image](examples/stella.jpeg)

WebP with sparkles:

![WebP with sparkles](examples/stella.webp)

Gif with sparkles:

![Gif with sparkles](examples/stella.gif)

The gif is smaller because gif files are huge :/

---

Combo for very pretty sparkles: sparkles.png + sparkle3.gif + sparkle9.gif

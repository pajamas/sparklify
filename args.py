import argparse

parser = argparse.ArgumentParser(
                    prog = './sparklify',
                    description = 'A program for making images into sparkly gifs',
                    epilog = 'https://gitlab.com/pajamas/sparklify')

parser.add_argument('image_path')
parser.add_argument('-f', '--format', default='webp', choices=['webp', 'gif'],
        help='output file format, either webp or gif. default webp')
parser.add_argument('-o', '--output-file', dest='output', metavar='FILENAME', 
        help='name of output file')
parser.add_argument('-m', '--mask', help='path to mask file')
parser.add_argument('-d', '--draw-mask', help='draw mask', dest='draw', action='store_true')
parser.add_argument('-r', '--resize', type=float, 
        help='ratio for resizing input image. ')
parser.add_argument('-n', '--no-lighten', action='store_true', dest='no_lighten',
        help='don\'t lighten sparkly parts of image')
parser.add_argument('-b', '--bubbles', action='store_true',
        help='add bubbles to top half of image')

args = parser.parse_args()

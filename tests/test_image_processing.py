from PIL import Image
from ..image_processing import resize_image

def new_image(size: tuple[int, int]) -> Image:
    return Image.new("RGBA", size)

def test_resize_image_by_ratio():
    """"""
    ratio_test_values = [
        ((10,10), 2, (20,20)),
        ((345, 231), 0.7, (241, 161))
    ]

    for test in ratio_test_values:
        image = new_image(test[0])
        resized_image = resize_image(image, ratio=test[1])

        assert image.size == test[0], "original image size changed"
        assert resized_image.size == test[2]

def test_resize_image_by_dimensions():
    dimensions_test_values = [
        ((100,100), (236,94), (236,94)),
        ((95,371), (200,None), (200,781)),
        ((843,556), (None,10), (15,10))
    ]

    for test in dimensions_test_values:
        image = new_image(test[0])
        resized_image = resize_image(image, dimensions=test[1])

        assert image.size == test[0], "original image size changed"
        assert resized_image.size == test[2]

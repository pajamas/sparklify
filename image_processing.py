from PIL import Image
from typing import Callable

def add_texture(background: Image, texture: Image) -> Image:
    """Creates an alpha composite of background and texture, where texture is
    repeated so it covers the whole background."""

    result: Image = background.copy()

    for i in range(result.width // texture.width + 1):
        for j in range(result.height // texture.height + 1):
            result.alpha_composite(texture, dest=(i*texture.width, j*texture.height))

    return result

def merge_images_by_mask(
        background: Image,
        foreground: Image,
        mask: Image
        ) -> Image:
    """Merge images according to a mask, where darkest pixels show background
    and lightest pixels show foreground"""

    result = background.copy()

    # convert mask to grayscale
    mask = mask.convert("L")

    result.paste(foreground, mask=mask)

    return result

def apply_filters_to_channels(
        image: Image,
        filters: dict[str, Callable[[int], int]]
        ) -> Image:
    """Applies filters to different channels in the image and returns a new
    image with the filtered channels.

    params:
    - image: the image to be filtered
    - filters: a dictionary with channel names (e.g. "R") as keys and functions
      as values
    """

    image_channels = image.getbands()

    result_channels = {
        name: image.getchannel(name) for name in image_channels
    }

    for channel_name in filters:
        channel = result_channels[channel_name]
        filtered = Image.eval(channel, filters[channel_name])
        result_channels[channel_name] = filtered

    result = Image.merge(image.mode, [result_channels[name] for name in image_channels])
    return result

def convert_red_to_alpha(image: Image) -> Image:
    """Returns white image with the source image's red channel as alpha
    channel

    This function is used for converting sparkle gif frames with black
    backgrounds to transparent ones.
    """

    white_image = Image.new("RGBA", image.size, (255,255,255))

    red_channel = image.getchannel("R")
    white_image.putalpha(red_channel)

    return white_image

def resize_image(image: Image,
                 ratio: float = None,
                 dimensions: tuple[int] = None
                 ) -> Image:
    """Resize image either by ratio or to specific dimensions.

    Params:
        - ratio: 1 for same size, 2 for doubled width and height, etc.
        - dimensions: tuple with one or both dimensions. If only one
          dimension is specified, the other one will be calculated to conserve
          aspect ratio. Examples: (100, 350), (None, 500)
    """

    if ratio:
        image = image.resize((
            int(image.width * ratio),
            int(image.height * ratio)
        ))
    elif dimensions:
        new_width = dimensions[0]
        new_height = dimensions[1]
        if not dimensions[0]:
            new_width = int(image.width * new_height/image.height)
        if not dimensions[1]:
            new_height = int(image.height * new_width/image.width)

        image = image.resize((new_width, new_height))

    return image

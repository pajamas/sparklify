class Pixels:
    def add(p1,p2):
        r = int(p1[0]*(1-p2[3]) + p2[0]*p2[3])
        g = int(p1[1]*(1-p2[3]) + p2[1]*p2[3])
        b = int(p1[2]*(1-p2[3]) + p2[2]*p2[3])
        return (r,g,b,int(min(255,p1[3]+p2[3])))

    def _close_to_old(p1,p2):
        if (p1[0] < p2[0]-40 or p1[0] > p2[0]+40):
            return False
        if (p1[1] < p2[1]-40 or p1[1] > p2[1]+40):
            return False
        if (p1[2] < p2[2]-40 or p1[2] > p2[2]+40):
            return False
        return True

    def is_dark(pixel):
        return sum(pixel[0:3]) < 30

    def is_transparent(pixel):
        return pixel[3] < 5

    def is_white(pixel):
        return sum(list(frame_pixel[0:3])) == 3*255

    def lightness_to_alpha(pixel):
        return (255,255,255,(sum(pixel[0:3])/(3*255))*pixel[3]/255)

    def lightness_to_alpha_R(pixel):
        return (255,255,255,(pixel[0]/(255))*pixel[3]/255)

    def lightness_to_alpha_G(pixel):
        return (255,255,255,(pixel[1]/(255))*pixel[3]/255)

    def lightness_to_alpha_B(pixel):
        return (255,255,255,(pixel[2]/(255))*pixel[3]/255)

    def close_to(p2):
        def f(p1):
            if (p1[0] < p2[0]-40 or p1[0] > p2[0]+40):
                return False
            if (p1[1] < p2[1]-40 or p1[1] > p2[1]+40):
                return False
            if (p1[2] < p2[2]-40 or p1[2] > p2[2]+40):
                return False
            return True
        return f

def create_pixel(bg_pixel, gif_pixel, x, y, frame):
    skip_gif = []
    skip_bg = []
    bg_operations = []
    gif_operations = [Pixels.lightness_to_alpha_R]
    operations = [Pixels.add]

    for skip in skip_bg:
        if skip(bg_pixel):
            return bg_pixel
    
    for skip in skip_gif:
        if skip(gif_pixel):
            return bg_pixel

    for operation in gif_operations:
        gif_pixel = operation(gif_pixel)

    for operation in bg_operations:
        bg_pixel = operation(bg_pixel)

    """
    # red pixels fade stars
    if (bg_pixel[0] > bg_pixel[2] and bg_pixel[0] > 100):
        new_alpha = gif_pixel[3]
        diff = bg_pixel[0] - bg_pixel[2]
        diff = diff/255
        new_alpha = (new_alpha+diff)/2
        gif_pixel = (255,255,255,new_alpha)
    """

    if mask != None:
        mask_pixel = mask.getpixel((x,y))
        intensity = mask_pixel[0]/255
    else:
        intensity = 1

    gif_pixel = (255,255,255,gif_pixel[3]*intensity)

    new_pixel = bg_pixel
    #if not args.no_lighten:
    #    new_pixel = new_pixel[0] + 5*intensity,new_pixel[1] + 5*intensity, new_pixel[2] + 2.5*intensity, new_pixel[3]

    for operation in operations:
        new_pixel = operation(new_pixel, gif_pixel)

    """
    # close to mountains fade stars
    if y > frame.height*0.8:
        gif_pixel = (255,255,255,gif_pixel[3]*((frame.height*0.05-(y-frame.height*0.8))/(frame.height*0.05)))
    """
    return new_pixel

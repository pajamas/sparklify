import sys
import argparse
import numpy as np
import PIL
import math
import tkinter
import wx
from tkinter import ttk
from PIL import Image, ImageTk
from io import BytesIO
from PIL import ImageFilter
from PIL import ImageEnhance
from PIL import ImageSequence
from PIL import GifImagePlugin

class Drawing():
    def __init__(self, bg):
        self.bg = bg
        self.is_pressed = False
        self.old_pos = (0,0)
        self.pos = (0,0)
        self.size = int(min(self.bg.size)/10)
        self.window = tkinter.Tk()
        self.window.title("Create mask | Sparklify")
        self.root = ttk.Frame(self.window, style="Light.TFrame", padding=20)
        self.root.pack()
        self.brush_images = [Image.open("resources/brush2.png").convert("RGBA")]#, Image.open("resources/brush2.png").convert("RGBA")]
        self.brush_moved = False

        #icon1 = PhotoImage(file = "resources/brush.png")


        self.lightness_enhancer = ImageEnhance.Brightness(self.brush_images[0])
        self.brush_image = self.brush_images[0].resize((self.size, self.size))

        help_text = ttk.Label(self.root, text="Please draw the mask. Dark parts will have no sparkles, and lightest parts will have the brightest sparkles.", style="Light.TLabel")
        help_text['wraplength'] = max(40, self.bg.width)
        help_text.pack()

        self.menu_frame = ttk.Frame(self.root, style="Light.TFrame")
        self.menu_frame.pack(expand=True, fill="x")
        self.menu_frame.columnconfigure(1, weight=0)
        self.menu_frame.columnconfigure(2, weight=1)
        self.menu_frame.columnconfigure(4, weight=0)

        ttk.Label(self.menu_frame, text="Size:", style="Light.TLabel").grid(row=0, column=0, sticky=tkinter.W)

        self.size_entry = ttk.Spinbox(self.menu_frame,from_=1,to=1000,increment=1,wrap=False,command=self.enter_size, width=6)
        self.size_entry.bind('<KeyRelease>', self.enter_size)
        self.size_entry.set(self.size)
        self.size_entry.grid(row=0, column=1)

        ttk.Button(self.menu_frame, text="Export mask", command=self.export_mask, style='Cute2.TButton').grid(row=0, column=2, sticky=tkinter.E)
        ttk.Button(self.menu_frame, text="Import mask", command=self.import_mask, style='Cute2.TButton').grid(row=0, column=3, sticky=tkinter.E)
        ttk.Button(self.menu_frame, text="Sparklify", command=self.stop, style='Cute.TButton').grid(row=0, column=4, sticky=tkinter.E)

        self.canvas = tkinter.Canvas(self.root, height=self.bg.height, width=bg.width, background="white")
        self.canvas.pack()

        sliders_container = ttk.Frame(self.root)
        sliders_container.pack()

        ttk.Label(sliders_container, text="Lightness:", style="Light.TLabel").grid(row=0, column=0)
        self.lightness_scale = ttk.Scale(sliders_container, from_=0, to=1, orient=tkinter.HORIZONTAL, command=self.change_lightness, style="Cute.Horizontal.TScale")
        self.lightness_scale.grid(row=0,column=1)
        self.lightness_scale.set(1)
        ttk.Label(sliders_container, text="Opacity:", style="Light.TLabel").grid(row=0, column=2)
        self.opacity_scale = ttk.Scale(sliders_container, from_=0, to=1, orient=tkinter.HORIZONTAL, style='Cute.Horizontal.TScale')
        self.opacity_scale.grid(row=0,column=3)
        self.opacity_scale.set(1)

        self.transparent = Image.new(mode="RGBA", size=self.bg.size, color=(0,0,0,0))

        self.bg_image = ImageTk.PhotoImage(bg)
        self.canvas.create_image(0,0,anchor=tkinter.NW,image=self.bg_image)

        self.mask = Image.new(mode="RGBA", size=self.bg.size, color=(0,0,0,255))
        self.mask_with_alpha = self.mask.copy()
        self.mask_with_alpha.putalpha(128)
        self.mask_image = ImageTk.PhotoImage(self.mask_with_alpha)
        self.mask_container = self.canvas.create_image(0,0,anchor=tkinter.NW,image=self.mask_image)

        self.new_stroke = Image.new(mode="RGBA", size=self.bg.size, color=(0,0,0,0))
        self.stroke_with_opacity = None
        self.stroke_image = ImageTk.PhotoImage(self.new_stroke)
        self.stroke_container = self.canvas.create_image(0,0,anchor=tkinter.NW,image=self.stroke_image)

        self.canvas.bind('<Button-1>', self.on_click)
        self.canvas.bind('<ButtonRelease-1>', self.merge_stroke)
        self.canvas.bind('<B1-Motion>', self.motion)

        s = ttk.Style()
        s.configure('Cute.TButton', background='#ffadd9')
        s.map('Cute.TButton', background=[('active', '#ffbadf')])
        s.configure('Cute2.TButton', background='#fffdc2')
        s.map('Cute2.TButton', background=[('active', '#fffecc')])
        s.configure('Light.TFrame', background='white')
        s.configure('Light.TLabel', background='white')
        s.configure('Cute.Horizontal.TScale', background='#ffadd9', troughcolor="#fffdc2")
        s.map('Cute.Horizontal.TScale',
                background=[('active', '#ffbadf')])

    def merge_stroke(self, event):
        if not self.brush_moved:
            self.draw_brush(event.x, event.y)
        transparent = self.new_stroke.copy()
        transparent.putalpha(0)
        transparent_stroke = Image.blend(transparent, self.new_stroke, self.opacity_scale.get())
        #self.mask = Image.blend(self.mask, self.new_stroke, self.opacity_scale.get())
        self.mask.alpha_composite(transparent_stroke, dest=(0,0))
        self.new_stroke = Image.new(mode="RGBA", size=self.bg.size, color=(0,0,0,0))
        self.update_stroke()
        self.update_mask()

    def change_lightness(self, event):
        self.update_brush_image()

    def enter_size(self, event=None):
        try:
            self.size = int(self.size_entry.get())
            self.update_brush_image()
        except ValueError:
            pass
    def update_brush_image(self):
        self.brush_image = self.lightness_enhancer.enhance(self.lightness_scale.get()).resize((self.size, self.size))

    def get_mask(self):
        print("Please draw the mask. Dark parts will have no sparkles, and lightest parts will have the brightest sparkles.")
        self.window.mainloop()
        return self.mask

    def stop(self):
        self.window.destroy()

    def update_mask(self):
        self.mask_with_alpha = self.mask.copy()
        self.mask_with_alpha.putalpha(128)
        self.mask_image = ImageTk.PhotoImage(self.mask_with_alpha)
        self.canvas.itemconfig(self.mask_container, image=self.mask_image)
        self.canvas.imgref = self.mask_image

    def update_stroke(self):
        transparent = self.new_stroke.copy()
        transparent.putalpha(0)
        transparent_stroke = Image.blend(transparent, self.new_stroke, self.opacity_scale.get())
        self.stroke_image = ImageTk.PhotoImage(transparent_stroke)
        self.canvas.itemconfig(self.stroke_container, image=self.stroke_image)
        self.canvas.imgref = self.stroke_image


    def draw_brush(self,x,y):
        self.new_stroke.alpha_composite(self.brush_image, dest=(int(x-self.size/2), int(y-self.size/2)))

    def draw_brush_line(self):
        line_length = math.sqrt((self.pos[0]-self.old_pos[0])**2 + (self.pos[1]-self.old_pos[1])**2)
        diffs = (self.pos[0]-self.old_pos[0], self.pos[1]-self.old_pos[1])
        for i in range(0,int(line_length),max(int(self.size/6),1)):
            self.draw_brush(self.old_pos[0] + i*diffs[0]/line_length, self.old_pos[1] + i*diffs[1]/line_length)
        self.update_stroke()


    def on_click(self,event):
        self.old_pos = self.pos
        self.pos = (event.x, event.y)
        self.brush_moved = False
        #self.draw_brush(event.x, event.y)
        """self.mask_with_alpha = self.mask.copy()
        self.mask_with_alpha.putalpha(128)
        self.mask_image = ImageTk.PhotoImage(self.mask_with_alpha)
        self.canvas.itemconfig(self.mask_container, image=self.mask_image)
        self.canvas.imgref = self.mask_image"""

    def motion(self,event):
        self.old_pos = self.pos
        self.pos = (event.x, event.y)
        self.draw_brush_line()
        self.brush_moved = True

    def get_path(self, wildcard, prompt, style):
        app = wx.App(None)
        dialog = wx.FileDialog(None, 'Export mask', wildcard=wildcard, style=style)
        if dialog.ShowModal() == wx.ID_OK:
            path = dialog.GetPath()
        else:
            path = None
        dialog.Destroy()
        return path

    def export_mask(self):
        filename = self.get_path("PNG image (.png)|*.png|JPG image (*.jpg)|*.jpg", "Export mask", wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
        if filename == None:
            return
        self.mask.save(filename)

    def import_mask(self):
        filename = self.get_path("All supported formats|*.png;*.jpg;*.jpeg;*.bmp;*.webp", "Import mask", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if filename == None:
            return
        self.mask = Image.open(filename).convert("RGBA").resize(self.mask.size)
        self.update_mask()
